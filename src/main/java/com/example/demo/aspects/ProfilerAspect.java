/*
 * Algebra labs.
 */

package com.example.demo.aspects;

import java.text.NumberFormat;

import org.aspectj.lang.ProceedingJoinPoint;

public class ProfilerAspect {

	public Object profile(ProceedingJoinPoint joinPoint) throws Throwable {

		double start = System.nanoTime();
		Object returnValue = joinPoint.proceed();
		elapsed(start, joinPoint);

		return returnValue;
	}

	private void elapsed(double start, ProceedingJoinPoint joinPoint) {
		double elapsed = (System.nanoTime() - start) / secondAsNanos;
		System.out.println(joinPoint.getStaticPart().toShortString()
				+ ": elapsed time in seconds " + fmt.format(elapsed));
	}

	final static long secondAsNanos = 1000000000;
	final static NumberFormat fmt = NumberFormat.getNumberInstance();
	static {
		fmt.setMinimumFractionDigits(10);
	}

}