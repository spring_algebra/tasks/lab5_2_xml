/*
 * Algebra labs.
 */

package com.example.demo.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.example.demo.service.Catalog;

public class UT_Catalog {

	@Test
	public void catalogTest() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("configuration/applicationContext.xml");
		assertTrue("spring container should not be null", ctx != null);

		Catalog catalog = ctx.getBean(Catalog.class);

		
		System.out.println("Catalog size: " + catalog.size());
		System.out.println(catalog.findById(1L));

		ctx.close();
	}

}

